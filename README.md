# Spring Boot Prometheus

### Things to do list

1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-prometheus.git`
2. Navigate to the folder: `cd spring-boot-prometheus`
3. Run docker compose: `docker compose -f ./prometeus-config/docker-compose.yml up`
4. Run the application: `mvn clean spring-boot:run`

### Images Screen shot

Prometheus Target

![Prometheus Target](img/target.png "Prometheus Target")

Prometheus Graph

![Prometheus Graph](img/graph.png "Prometheus Graph")
